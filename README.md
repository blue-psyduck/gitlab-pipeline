# Gitlab Pipeline

This repository contains the Gitlab Pipeline files used by the other repositories of this group.

## Go Packages

The file `preset/go-package.yml` is intended for repositories containing Go packages, without the need to deploy
anything.

### Parameters

| Name       | Default value | Description                               |
|------------|---------------|-------------------------------------------|
| GO_VERSION | "1.19"        | The version of Go to use in the Pipeline. |
| PACKAGES   | "./pkg/..."   | The list of packages to use for coverage. |
